var passport = require('passport');
var async = require('async');
var _ = require('lodash');
var LocalStrategy = require('passport-local').Strategy;

exports.setup = function(User, config) {
    passport.use(new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password' // this is the virtual field on the model
        },
        function(email, password, done) {
            User.findOne({
                email: email.toLowerCase()
            }, function(err, userdetail) {
                if (err)
                    return done(err);
                if (!userdetail) {
                    return done(null, false, { message: 'ERROR_EMAIL_NOT_REGISTERED' });
                }
                if (!userdetail.authenticate(password)) {
                    return done(null, false, { message: 'ERROR_INVALID_EMAIL_PWD' });
                }
                getChatList(userdetail, done);
            });
        }
    ));
};

function getChatList(user, done_fn) {
    var Chat = require('../../models/chat.model');
    Chat.find({
        $and: [
            { $or: [{ from_user: user._id }, { to_user: user._id }] },
            { $and: [{ user: user._id }] }
        ]
    }).lean().populate('from_user', 'user.first_name facebook.address').exec(function(err, chats) {
        if (err) {
            return done_fn(null, user);
        } else {
            var user_list = [];
            var user_new = [];
            var total_unread_msg = 0;

            if (chats.length <= 0) {
                return done_fn(null, user);
            }
            // console.log(chats);
            async.eachSeries(chats, function iterator(msg, callback) {
                if (msg && msg.from_user && msg.to_user) {
                    if (msg.from_user._id !== user._id && msg.to_user._id.toString() === user._id.toString()) {
                        Chat.count({ "to_user": user._id, "from_user": msg.from_user._id, "status": 1, "user": user._id }, function(err, msg_cnt) {
                            var tmpObj = {
                                user_name: msg.from_user.first_name + " " + msg.from_user.last_name,
                                avatar: msg.from_user.avatar,
                                id: msg.from_user._id.toString(),
                                unread_cnt: msg_cnt
                            };
                            if (msg.status === 1) {
                                total_unread_msg = parseInt(total_unread_msg) + 1;
                            }
                            if (_.findIndex(user_list, { id: user._id }) === -1) {
                                user_list.push(tmpObj);
                            }
                            callback(null, msg);
                        });
                    } else if (msg.to_user._id.toString() !== user._id.toString() && msg.from_user._id.toString() === user._id.toString()) {
                        Chat.count({ "to_user": user._id, "from_user": msg.to_user._id, "status": 1, "user": user._id }, function(err, msg_cnt) {
                            var tmpObj = {
                                user_name: msg.to_user.first_name + " " + msg.to_user.last_name,
                                avatar: msg.to_user.avatar,
                                id: msg.to_user._id.toString(),
                                unread_cnt: msg_cnt
                            };
                            if (_.findIndex(user_list, { id: user._id }) === -1) {
                                user_list.push(tmpObj);
                            }
                            callback(null, msg);
                        });
                    }
                } else {
                    callback()
                }
            }, function done() {
                user_list = _.uniq(user_list, 'id');
                var user_new = JSON.parse(JSON.stringify(user));
                user_new.chat_history = user_list;
                user_new.total_unread = total_unread_msg;
                //                console.log(user_new);
                return done_fn(null, user_new, null);
            });
        }
    });
};
// module.exports=getChatList;
exports.getChatList = getChatList;