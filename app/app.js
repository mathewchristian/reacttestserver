/**
 * Main application file
 */

'use strict';

// Set default node environment to development
process.env.NODE_ENV = process.env.NODE_ENV || 'development';
process.env.TZ = 'Asia/Kolkata';

var express = require('express');
var config = require('./config/environment');
var cors = require('cors');
var server = require('http').Server(express);
var usernames = {};


// Setup server
var app = express();
var server = require('http').createServer(app);

var path = require('path');
app.use(express.static('public'));
console.log(__dirname);
app.use('/get_file/', express.static(path.join(__dirname, '../public/invoice_pdf/')));

function logger(req, res, next) {
    var headers = {};
    // IE8 does not allow domains to be specified, just the *
    // headers["Access-Control-Allow-Origin"] = req.headers.origin;
    headers["Access-Control-Allow-Origin"] = "*";
    headers["Access-Control-Allow-Methods"] = "POST, GET, PUT, DELETE, OPTIONS";
    headers["Access-Control-Allow-Credentials"] = false;
    headers["Access-Control-Max-Age"] = '86400'; // 24 hours
    header('Access-Control-Allow-Headers: Authorization, Content-Type');

    if (req.method === 'OPTIONS') {
        console.log('!OPTIONS');
        res.writeHead(200, headers);
        //res.end();
        res.send();
    } else {
        next();
    }
}
app.use(cors());
//app.use(logger);

require('./config/express')(app);
require('./routes')(app);

function checkAuth(err, req, res, next) {
    if (err.name.indexOf('Unauthorized') !== -1) {
        res.status(401);
        res.send({ data: null, is_error: true, message: "Unauthorized Request, Please sign in first." });
    } else {
        return next();
    }
}
app.use(checkAuth)
// Start server

app.get('/', function(req, res) {
    res.send("NodeDemo running on port : " + config.port);
});

server.listen(config.port, config.ip, function() {
    console.log('Express server listening on %d, in %s mode', config.port, app.get('env'));
});

// Expose app
module.exports = app;
