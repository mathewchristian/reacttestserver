'use strict';

var mongoose = require('mongoose');
var autopopulate = require('mongoose-autopopulate');
var Schema = mongoose.Schema;
var crypto = require('crypto');
var mongooseHidden = require('mongoose-hidden')({ defaultHidden: { password: true } });
var async = require('async');
var _ = require('lodash');

var schemaOptions = {
  toObject: { virtuals: true },
  toJSON: { virtuals: true }
};

var FirmwaveSchema = new Schema({
  title: { type: String, default: '' },
  productImage: { type: String, default: '' },
  folder: { type: String, default: ''},
  firmwareFiles: {tyle: Array , default: []}
}, schemaOptions);

FirmwaveSchema.plugin(autopopulate);
FirmwaveSchema.plugin(mongooseHidden);
module.exports = mongoose.model('Firmware', FirmwaveSchema);
