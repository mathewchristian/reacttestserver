'use strict';

var mongoose = require('mongoose');
var autopopulate = require('mongoose-autopopulate');
var Schema = mongoose.Schema;
var crypto = require('crypto');

var authTypes = ['github', 'twitter', 'facebook', 'google', 'linkedin'];

var mongooseHidden = require('mongoose-hidden')({ defaultHidden: { password: true } });

var async = require('async');
var _ = require('lodash');

var schemaOptions = {
  toObject: { virtuals: true },
  toJSON: { virtuals: true }
};

var UserSchema = new Schema({
  email: { type: String, lowercase: true },
  hashedPassword: { type: String, hide: true },
  salt: { type: String, hide: true },
  create_date: { type: Date, default: Date.now },
  last_updated: { type: Date, default: null },
  is_deleted: { type: Boolean, default: false },
  country_code: { type: String, default: null },
  phone_number: { type: String, default: null },
  avatar: { type: String, default: '' },
  gender: { type: String, default: '' },
  address: { type: String, default: '' },
  city: { type: String, default: null },
  state: { type: String, default: null },
  country: { type: String, default: null },
  postcode: { type: String, default: null },
}, schemaOptions);

UserSchema.plugin(autopopulate);
UserSchema.plugin(mongooseHidden);

/**
 * Virtuals
 */
UserSchema
.virtual('password')
.set(function(password) {
  this._password = password;
  this.salt = this.makeSalt();
  this.hashedPassword = this.encryptPassword(password);
})
.get(function() {
  return this._password;
});

// Public profile information
UserSchema
.virtual('profile')
.get(function() {
  return {
    'name': this.name,
    'role': this.role
  };
});

UserSchema
.virtual('full_name')
.get(function() {
  return this.first_name + ' ' + this.last_name;
});

// Non-sensitive info we'll be putting in the token
UserSchema
.virtual('token')
.get(function() {
  return {
    '_id': this._id,
    'role': this.role
  };
});

/**
 * Validations
 */

// Validate empty email
UserSchema
.path('email')
.validate(function(email) {
  if (authTypes.indexOf(this.provider) !== -1)
    return true;
  return email.length;
}, 'ERROR_EMAIL_CAN_NOT_BLANK');

// Validate empty password
UserSchema
.path('hashedPassword')
.validate(function(hashedPassword) {
  if (authTypes.indexOf(this.provider) !== -1)
    return true;
  return hashedPassword.length;
}, 'ERROR_PWD_CAN_NOT_BLANK');

// Validate email is not taken
UserSchema
.path('email')
.validate(function(value, respond) {
  var self = this;
  this.constructor.findOne({ email: value }, function(err, user) {
    if (err)
      throw err;
    if (user) {
      if (self.id === user.id)
        return respond(true);
      return respond(false);
    }
    respond(true);
  });
}, 'ERROR_EMAIL_IN_USE');

var validatePresenceOf = function(value) {
  return value && value.length;
};

UserSchema
.post('init', function(model) {
  this.wasNew = this.isNew;
  this.is_updated = false;
});
/**
 * Methods
 */
UserSchema.methods = {
  /**
   * Authenticate - check if the passwords are the same
   *
   * @param {String} plainText
   * @return {Boolean}
   * @api public
   */
  authenticate: function(plainText) {
    console.log(plainText);
    console.log(this.hashedPassword);
    return this.encryptPassword(plainText) === this.hashedPassword;
  },
  /**
   * Make salt
   *
   * @return {String}
   * @api public
   */
  makeSalt: function() {
    return crypto.randomBytes(16).toString('base64');
  },
  /**
   * Encrypt password
   *
   * @param {String} password
   * @return {String}
   * @api public
   */
  encryptPassword: function(password) {
    if (!password || !this.salt)
      return '';
    console.log(this.salt)
    var salt = new Buffer(this.salt, 'base64');
    var pwd = crypto.pbkdf2Sync(password, salt, 10000, 64,'sha512').toString('base64');
    console.log(pwd);
    return pwd;
  }
};

module.exports = mongoose.model('User', UserSchema);
