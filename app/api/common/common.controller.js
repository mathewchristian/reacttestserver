var _ = require('lodash');
var mongoose = require("mongoose");
var validator = require('validator');
var unrestricted_to_company = ['Company', 'User'];

// Get list of things
exports.get = function(req, res) {
    var col = req.params.collection;
    var where = {};
    if (req.companyId) {
        var result = _.some(unrestricted_to_company, col);
        if (result.length <= 0) {
            where = { $or: [{ company: req.companyId }, { company: null }] };
        }
    }
    var Model = mongoose.model(req.params.collection);
    Model.find({ $and: [{ $or: [{ is_deleted: false }, { is_deleted: null }] }, where] }, function(err, annos) {
        if (err) {
            res.send(send_response(null, true, parse_error(err)));
        } else {
            res.send(send_response(annos));
        }
    });
};

// Get a single thing
exports.getById = function(req, res) {
    var Model = mongoose.model(req.params.collection);
    var json = { _id: req.params.id };
    if (req.params.collection === 'User') {
        var bl = validator.isEmail(req.params.id);
        if (bl === true) {
            json = { email: req.params.id };
        }
    }
    Model.findOne(json, function(err, annos) {
        if (err) {
            res.send(send_response(null, true, parse_error(err)));
        } else {
            res.send(send_response(annos));
        }
    });
};

// Creates a new thing in the DB.
exports.createnew = function(req, res) {
    if (req.companyId) {
        if (req.params.collection !== 'Company') {
            req.body.company = req.companyId;
        }
    }
    var Model = mongoose.model(req.params.collection);
    var data = new Model(req.body);
    Model.create(data, function(err, mod) {
        if (err) {
            res.send(send_response(null, true, parse_error(err)));
        } else {
            res.send(send_response(mod));
        }
    });
};

// Updates an existing thing in the DB.
exports.update = function(req, res) {
    var Model = mongoose.model(req.params.collection);
    var id = req.params.id;
    if (!id) {
        id = req.body._id;
    }
    if (req.body._id) {
        delete req.body._id;
    }
    Model.findById(id, function(err, thing) {
        if (err) {
            res.send(send_response(null, true, parse_error(err)));
            return;
        }
        if (!thing) {
            res.send(send_response(null, true, "ERROR_NO_DATA"));
            return;
        }
        //var updated = _.merge(thing, req.body);
        var updated = _.assign(thing, req.body);
        if (!updated) {
            res.send(send_response(null, true, "ERROR_NO_DATA"));
            return;
        }
        updated.save(function(err) {
            if (err) {
                res.send(send_response(null, true, parse_error(err)));
            } else {
                res.send(send_response(thing));
            }
        });
    });
};

exports.putOrPost = function(req, res) {
    if (req.body._id) {
        exports.update(req, res);
    } else {
        exports.createnew(req, res);
    }
};

exports.softdestroy = function(req, res) {
    var Model = mongoose.model(req.params.collection);
    var id = req.params.id;

    Model.findById(id, function(err, thing) {
        if (err) {
            res.send(send_response(null, true, parse_error(err)));
        }
        if (!thing) {
            res.send(send_response(null, true, "ERROR_NO_DATA"));
        }
        thing.is_deleted = true;
        thing.save(function(err) {
            if (err) {
                res.send(send_response(null, true, parse_error(err)));
            } else {
                res.send(send_response(thing));
            }
        });
    });
};

// Deletes a thing from the DB.
exports.destroy = function(req, res) {
    var Model = mongoose.model(req.params.collection);
    Model.findById(req.params.id, function(err, thing) {
        if (err) {
            return res.send(send_response(null, true, parse_error(err)));
        }
        if (!thing) {
            return res.send(send_response(null, true, "ERROR_NO_DATA"));
        }
        thing.remove(function(err) {
            if (err) {
                return res.send(send_response(null, true, parse_error(err)));
            }
            return res.status(204).send(send_response({}));
        });
    });
};
/**
 * limit = {skip:0, limit:10}
 * sort = {"fild_name" : -1/1}
 * */
exports.executeQuery = function(req, res) {
    var Model = mongoose.model(req.params.collection);
    var where = req.body.where;
    var populate = req.body.populate;
    var fields = req.body.fields;
    var sort = req.body.sort;
    var limit = req.body.limit;
    var in_cls = req.body.in;
    var count = req.body.count;
    if (!isEmpty(where) && !isEmpty(in_cls)) {
        res.send(send_response(null, true, "You can not send both WHERE and IN condition togather."));
        return;
    }
    if (!fields) {
        fields = '';
    }
    if (req.params.collection === 'User') {
        fields = fields + " -hashedPassword -salt";
    }
    var query = Model.find({}, fields);
    if (!isEmpty(where)) {
        query = Model.find(where, fields);
    }
    if (!isEmpty(in_cls)) {
        var temp = {};
        temp[in_cls.key] = {
            $in: in_cls.val
        };
        query = Model.find(temp, fields);
    }

    if (populate) {
        query = query.populate(populate);
    }
    if (sort) {
        query = query.sort(sort);
    }
    if (!isEmpty(limit)) {
        query = query.skip(limit.skip).limit(limit.limit);
    }
    if (count === true) {
        query.count().exec(function(err, annos) {
            if (err) {
                res.send(send_response(null, true, parse_error(err)));
            } else {
                res.send(send_response(annos));
            }
        });
    } else {
        query.exec(function(err, annos) {
            if (err) {
                console.log(err)
                res.send(send_response(null, true, parse_error(err)));
            } else {
                res.send(send_response(annos));
            }
        });
    }
};
