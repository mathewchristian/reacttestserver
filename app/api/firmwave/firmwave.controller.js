var _ = require('lodash');
var config = require('../../config/local.env');
var path = require('path');
var request = require('request');
var qs = require('querystring');

//requiring path and fs modules
const fs = require('fs');

//joining path of directory
const directoryPath = path.join(__dirname, 'firmwareFiles');

exports.FirmwaveGet= function(req, res) {
  var filesList;
  //passsing directoryPath and callback function
  fs.readdir(directoryPath, function (err, files) {
    filesList = files.filter(function(e){
      return path.extname(e).toLowerCase() === '.gz'
    });
    let config = {
      sections: [
        {
          title: 'VCU-Base',
          productImage: 'img/VCU-Base.png',
          folder: 'firmwares/VCU-Base',
          firmwareFiles: filesList
        }
      ]
    }
      res.json({ data: config, is_error: false });
  });
};
