'use strict';

var express = require('express');
var controller = require('./firmwave.controller');
var config = require('../../config/environment');
var router = express.Router();

router.get('/getall',controller.FirmwaveGet)
module.exports = router;
