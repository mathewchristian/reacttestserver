'use strict';

// Test Password
var crypto = require('crypto');

var salt = crypto.randomBytes(16).toString('base64');
var pwd = crypto.pbkdf2Sync("abc", salt, 10000, 64).toString('base64');
console.log("Salt : " + salt);
console.log("Password : " + pwd);
