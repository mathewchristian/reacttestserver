/**
 * Socket.io configuration
 */

'use strict';

var config = require('./environment');
var mongoose = require("mongoose");
var moment = require("moment");

// When the user disconnects.. perform this
function onDisconnect(socket) {}

// When the user connects.. perform this
function onConnect(socket) {
    // When the client emits 'info', this listens and executes
    socket.on('info', function(data) {
        console.info('[%s] %s', socket.address, JSON.stringify(data, null, 2));
    });

    // Insert sockets below
    //  require('../api/thing/thing.socket').register(socket);
}
var rooms = [];
var usernames = {};

module.exports = function(socketio) {

    // socket.io (v1.x.x) is powered by debug.
    // In order to see all the debug output, set DEBUG (in server/config/local.env.js) to including the desired scope.
    //
    // ex: DEBUG: "http*,socket.io:socket"

    // We can authenticate socket.io users and access their token through socket.handshake.decoded_token
    //
    // 1. You will need to send the token in `client/components/socket/socket.service.js`
    //
    // 2. Require authentication here:
    // socketio.use(require('socketio-jwt').authorize({
    //   secret: config.secrets.session,
    //   handshake: true
    // }));

    socketio.on('connection', function(socket) {
        socket.address = socket.handshake.address !== null ?
            socket.handshake.address.address + ':' + socket.handshake.address.port :
            process.env.DOMAIN;

        socket.connectedAt = new Date();

        // Call onDisconnect.
        socket.on('disconnect', function() {
            if (typeof rooms[socket.id] != 'undefined') {
                console.log(rooms[socket.id]['username'] + " is gone offline");
            }
            //            console.info('[%s] DISCONNECTED', socket.address);
        });

        socket.on("online", function(data) {
            //This socket event call once user login and ready for chat
            rooms[socket.id] = data;
            socket.join(data.room);
            var UserModel = mongoose.model("User");
            //var json = {_id: data.to_user};
            var date = moment(new Date());
            var json = { _id: data.room };
            UserModel.findOne(json, function(err, annos) {
                if (err) {
                    console.log(err)
                } else {
                    annos.last_login = date;
                    annos.save(function(err, savedc) {
                        if (err) {
                            console.log(err);
                        } else {}
                    });
                }
            });
        });

        socket.on("sendrecive", function(data) {
            //            socket.on("sendmymsg")
            socket.in(data.from_user).emit('receivemymessage', data);
        });

        // Call onConnect.
        onConnect(socket);
        //        console.log(socket.id);
        //        console.info(socket.id + '[%s] CONNECTED', socket.address);
    });
};
