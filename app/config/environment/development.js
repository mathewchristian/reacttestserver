'use strict';

// Development specific configuration
// ==================================
module.exports = {
    // MongoDB connection options
    mongo: {
       uri: 'mongodb://localhost:27017/devNode'
    },
    ip: process.env.OPENSHIFT_NODEJS_IP ||
        process.env.IP ||
        "0.0.0.0",
    // Server port
    port: process.env.OPENSHIFT_NODEJS_PORT ||
        process.env.PORT ||
        9000,
    seedDB: true
};
