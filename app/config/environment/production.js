'use strict';

// Production specific configuration
// =================================
module.exports = {
    // Server IP
    ip: process.env.OPENSHIFT_NODEJS_IP ||
        process.env.IP ||
        "0.0.0.0",
    // Server port
    port: process.env.OPENSHIFT_NODEJS_PORT ||
        process.env.PORT ||
        3052,
    seedDB: true,
    // MongoDB connection options
    mongo: {
        uri: 'mongodb://localhost:27017/devNode'

    }
};
