'use strict';
var nodemailer = require('nodemailer');
'use strict';
//const nodemailer = require('nodemailer');
var EmailTemplates = require('swig-email-templates');
var path = require('path'),
    templatesDir = path.join(__dirname, '../templates');
var mongoose = require("mongoose");
var async = require('async');


module.exports.send_mail = function(temp_url, data, res) {
    var transporter = nodemailer.createTransport('smtps://abc.php%40gmail.com:abc@smtp.gmail.com');
    var SystemSettings = mongoose.model('SystemSettings');
    console.log('a');
    var content_filter = ["FACEBOOK_URL", "TWITTER_URL", "LINKEDIN_URL", "INSTAGRAM_URL", "GOOGLE_PLUS_URL"];

    SystemSettings.find({ content_type: { "$in": content_filter } }, function(err, icons) {
        console.log('b');
        async.eachSeries(icons, function iterator(icon, callback) {
            data.replace_var[icon.content_type] = icon.content_value;
            callback(null, icon);
        }, function done() {
            console.log('c');
            var templates = new EmailTemplates();
            templates.render(templatesDir + temp_url, data.replace_var, function(err, html, text) {
                var mailOptions = {
                    from: '"JBF" <support@example.com>', // sender address
                    to: data.send_to, // list of receivers
                    subject: data.subject, // Subject line
                    html: html // html body
                };
                console.log('d');
                // send mail with defined transport object
                // transporter.sendMail(mailOptions, (error, info) => {
                //     if (error) {
                //         return console.log(error);
                //     }
                //     console.log('Message %s sent: %s', info.messageId, info.response);
                // });
                //send mail with defined transport object
                transporter.sendMail(mailOptions, function(error, info) {
                    console.log('e');
                    if (error) {
                        console.log('f');
                        res({ data: error, is_error: true, message: 'error while sending email' });
                    }
                    console.log('g');
                    res({ data: data, is_error: false, message: 'Email sent' });
                    console.log('h');
                });
            })
        });
    });
};
